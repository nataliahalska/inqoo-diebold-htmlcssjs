/**
 * @typedef Product
 * @property {number} id
 * @property {string} name
 * @property {number} price_nett
 * @property {string} [description]
 */

/** @type Product[] */
var produts = [
  // { id: '123', name: 'Product Test 123', price_nett: 1230, description: '' },
  // { id: '234', name: 'Product Testing 234', price_nett: 2340, description: '' },
  // { id: '345', name: 'Product Example Test 345', price_nett: 3450, description: '' },
];

class ModelChanged extends Event {
  type = "model_changed";
}

class ProductsModel extends EventTarget {
  /**
   * @type Product[]
   * @protected
   */
  _data = [];

  /**
   * @type Product[]
   * @protected
   */
  _results = this._data;

  constructor(products) {
    super();
    this._data = products;
    this._results = this._data;
    this.filter("");
  }

  /**
   * @returns Product
   * @param {string} id
   */
  findById(id) {
    return this._data.find((prod) => prod.id == id);
  }

  /**
   *
   * @returns Product[]
   */
  getItems() {
    return this._results;
  }

  /**
   * Filer products
   * @param {string} query
   */
  filter(query) {
    // json-server ./db --static .

    // this._data =  loadJSON('products.json')
    // loadJSON('products.json', resp => {
    loadJSON("http://localhost:3000/products?q=" + query, ).then((resp) => {
      this._data = resp;
      this._results = this._data.filter((p) =>
        p.name.toLocaleLowerCase().includes(query.toLocaleLowerCase())
      );

      // show() / display() / disaptch? / etc.
      this.dispatchEvent(new ModelChanged("model_changed", this._results));
    });
  }
}

// function loadJSON(url, callback) {
//   const xhr = new XMLHttpRequest()
//   xhr.open('GET', url)
//   xhr.addEventListener('loadend', event => {
//     const data = JSON.parse(event.target.responseText)
//     //  console.log(3)
//     callback(data)
//   })
//   xhr.send()

//   //  console.log(1)
//   //  return data; // data does not exist yet
// }

//xhr
//load JSON ma dzialac tak jak echo
// resolve jest callbackie, jemu przekazuje wynik urla
// z xhr do callbacka

function loadJSON(url) {
  return new Promise((resolve) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.addEventListener("loadend", (event) => {
      const data = JSON.parse(event.target.responseText);
      //  console.log(3)
      resolve(data);
    });
    xhr.send();
  });
}
