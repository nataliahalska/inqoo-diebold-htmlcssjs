
class ProductEditorView {

  constructor(selector) {
    this.el = document.querySelector(selector)
    this.form = this.el.querySelector('form')

    this.form.addEventListener("submit", event=>{
      event.preventDefault()

      console.log(this.getData())
    })
  }

  /** 
   * Product data
   * @type Product
   * @protected
   */
  _data = {}

  /**
   * @param {Product} data 
   */
  setData(data) {
    this._data = data
    this.form.elements['name'].value = this._data['name']
    this.form.elements['price'].value = (this._data['price_nett'] / 100).toFixed(2)
  }

  /**
   * @returns Product
   */
  getData() {
    this._data['name'] = this.form.elements['name'].value
    this._data['price_nett'] = parseFloat(this.form.elements['price'].value) * 100

    return this._data
  }


}

