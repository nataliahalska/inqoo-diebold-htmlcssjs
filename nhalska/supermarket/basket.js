/** @type CartItem[] */
let cartItems = []
let cartTotal = 0

function cartReset() {
  cartItems = []
  cartTotal = 0
}

function cartAddProduct(product_id) {
  /** @type Product */
  const product = productsGetById(product_id)

  const item = cartItems.find(item => item.product_id === product_id)

  if (item) {
    item.amount++
    cartCalculateItem(item)
  } else {
    const amount = 1
    cartItems.push({
      // product: product,
      product_id,
      product,
      amount,
      subtotal: product.price * amount,
    })
  }
}

function cartCalculateItem(item) {
  item.subtotal = item.product.price * item.amount
}

function cartRemoveProduct(product_id) {
  const item = cartItems.find(item => item.product_id === product_id)

  if (item) {
    item.amount--
    cartCalculateItem(item)

    if (item.amount <= 0) {
      cartItems = cartItems.filter(i => i.product_id !== item.product_id)
    }
  }
}

/**
 * Find product By Id
 * @param {string} product_id 
 * @returns Product
 */
function productsGetById(product_id) {
  return products.find(product => product.id === product_id)
}

function cartGetItems() {
  return cartItems
}

/**  @type Product */
// var prod = productsGetById('123')
// prod.name.toUpperCase()

/**
 * @typedef Product
 * @property {string} id
 * @property {string} name
 * @property {number} price Nett price in cents
 */

/**
 * @typedef CartItem
 * @property {string} product_id
 * @property {Product} product
 * @property {number} amount
 * @property {number} subtotal
 */